﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpTestProject
{
    abstract class TcpConnectionBase
    {
        public TcpConnectionState ConnectionState { get; protected set; }
        public Socket ConnnectedSocket { get; protected set; }
        public abstract void SetupConnection(IPAddress ipAddress);
    }
}
