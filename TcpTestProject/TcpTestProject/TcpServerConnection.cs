﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpTestProject
{
    class TcpServerConnection: TcpConnectionBase
    {
        //This value should be set to a reasonable amount (usually between 2 and 5), based on how many connections one is realistically expecting and how quickly they can be Accepted.
        private const int BacklogNumber = 2;

        private readonly ManualResetEvent _acceptEventHandler = new ManualResetEvent(false);

        public TcpServerConnection()
        {
            ConnectionState = TcpConnectionState.NotInitialized;
        }

        public override void SetupConnection(IPAddress ipAddress)
        {
            // Establish the local endpoint for the socket 
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, GameConstants.Port);
            Socket listener = null;
            try
            {
                // Create a TCP/IP socket.  
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(localEndPoint);
                listener.Listen(BacklogNumber);

                // Start an asynchronous socket to listen for connections. 
                var acyncResult = listener.BeginAccept(AcceptCallback, listener);

                // Wait until a connection is made before continuing.  
                bool success = acyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(GameConstants.TimeToWait));
                if (!success)
                {
                    ConnectionState = TcpConnectionState.Failed;
                    TcpHelper.LogWaitServerError();
                }
                else
                {
                    success = _acceptEventHandler.WaitOne(TimeSpan.FromMilliseconds(GameConstants.TimeToWait));
                    if (!success)
                    {
                        ConnectionState = TcpConnectionState.Failed;
                        TcpHelper.LogWaitServerError();
                    }
                }
                _acceptEventHandler.Close();
                listener.Close();
            }
            catch (Exception e)
            {
                ConnectionState = TcpConnectionState.Failed;
                TcpHelper.LogSocketException(e);
                // Close Socket
                if (listener!=null)
                    listener.Close();
                _acceptEventHandler.Close();
            }

        }

        private void AcceptCallback(IAsyncResult acyncResult)
        {
            // return if listener Closed
            if (ConnectionState == TcpConnectionState.Failed)
                return;
            // Get the socket that handles the client request.  
            Socket listener = (Socket)acyncResult.AsyncState;
            try
            {
                ConnnectedSocket = listener.EndAccept(acyncResult);
                ConnectionState = TcpConnectionState.Connected;
                _acceptEventHandler.Set();
            }
            catch (Exception e)
            {
                ConnectionState = TcpConnectionState.Failed;
                TcpHelper.LogSocketException(e);
                // Close Socket
                listener.Close();
            }
        }
    }
}
