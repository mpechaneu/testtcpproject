﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TcpTestProject
{
    class NetworkTcpService
    {
        private readonly TcpMessageService _tcpMessageService;
        private readonly MessageSerializer _messageSerializer;
        private readonly MessageQueue _messageQueue;
        private bool _gameTerminated = false;

        public NetworkTcpService(bool server, IPAddress ipAddress, MessageQueue messageQueue)
        {
            _messageQueue = messageQueue;
            _messageSerializer = new MessageSerializer();
            _tcpMessageService = new TcpMessageService(server, ipAddress);
            _tcpMessageService.MessageArrived += OnMessageArrived;
            _tcpMessageService.SendFullGameState += SendFullGameState;
            _tcpMessageService.TerminateGameSession += TerminateGameSession;
        }

        public void Initialize()
        {
            _tcpMessageService.SetupConnection();
        }

        public void SendMessage(MessageBase message)
        {
            byte[] bytes = _messageSerializer.SerializeMessage(message);
            _tcpMessageService.SendMessage(bytes);
        }

        private void OnMessageArrived(byte[] message)
        {
            MessageBase messageObj = _messageSerializer.DeserealizeMessage(message);
            _messageQueue.AddMessage(messageObj);
            if (messageObj is TestMessage)
            {
                TestMessage obj = messageObj as TestMessage;
                Console.WriteLine(obj.Name);
                if (obj.Doubles != null)
                {
                    foreach (var value in obj.Doubles)
                    {
                        Console.WriteLine(value);
                    }
                }
            }
        }

        private void SendFullGameState()
        {
            Console.WriteLine("Restoring full game state");
            //SendMessage("You must restore state!");
        }

        private void TerminateGameSession()
        {
            _gameTerminated = true;
            Console.WriteLine("Terminate game session");
        }

        public bool IsGameTerminated()
        {
            return _gameTerminated;
        }
    }
}
