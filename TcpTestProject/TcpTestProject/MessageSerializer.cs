﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace TcpTestProject
{
    class MessageSerializer
    {
        public MessageSerializer()
        {
            
        }

        public byte[] SerializeMessage(MessageBase messageBase)
        {
            byte[] content = null;
            var formatter = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                if (GameConstants.CompressMessages)
                {
                    using (var ds = new DeflateStream(ms, CompressionMode.Compress, true))
                    {
                        formatter.Serialize(ds, messageBase);
                    }
                }
                else
                {
                    formatter.Serialize(ms, messageBase);
                }
                ms.Position = 0;
                content = ms.GetBuffer();
            }
            return content;
        }

        public MessageBase DeserealizeMessage(byte[] bytes)
        {
            MessageBase messageBase = null;
            var formatter = new BinaryFormatter();
            using (var ms = new MemoryStream(bytes))
            {
                if (GameConstants.CompressMessages)
                {
                    using (var ds = new DeflateStream(ms, CompressionMode.Decompress, true))
                    {
                        messageBase = (MessageBase)formatter.Deserialize(ds);
                    }
                }
                else
                {
                    messageBase = (MessageBase)formatter.Deserialize(ms);
                }
            }
            return messageBase;
        }
    }
}
