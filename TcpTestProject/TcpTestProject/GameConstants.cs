﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpTestProject
{
    class GameConstants
    {
        // Tcp and network
        public const int BufferSize = 2048; // size of buffer to recieve messages
        public const bool CompressMessages = true; // compress message before send
        public const int TimeToWait = 3000; // time for server socket to listen
        public const int ConnectTryCount = 1; // client tries to connect server
        public const int ClientTimeToWait = 3000; // time for client to connect to server + client sleep time
        public const int Port = 47268; // server socket listen port
        public const bool LifetimeTurnedOn = true; // send lifetime messages
        public const int InitializeTryCount = 6; // tries to connect between server and client (only for first try), during disconnect it's endless
        public const int SendLifetimeInterval = 1000; // ms interval to send lifetime messages
        public const int LifetimeIntervalTimout = 6000; // ms timeout to recieve lifetime message
        public const string ServerIp = "192.168.1.161"; // To remove when matchmaking server is ready.  
    }
}
