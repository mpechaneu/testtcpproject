﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpTestProject
{
    [Serializable]
    class TestMessage: MessageBase
    {
        public string Name;
        public List<double> Doubles;
    }
}
