﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpTestProject
{
    public enum TcpConnectionState
    {
        NotInitialized = 1,
        Failed = 2,
        Connected = 3,
        NeedRetry = 4,
        Disconnected = 5
    }
}
