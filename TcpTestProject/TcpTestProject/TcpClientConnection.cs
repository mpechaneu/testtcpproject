﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpTestProject
{
    class TcpClientConnection: TcpConnectionBase
    {
        private readonly ManualResetEvent _acceptEventHandler = new ManualResetEvent(false);

        public TcpClientConnection()
        {
            ConnectionState = TcpConnectionState.NotInitialized;
        }

        public override void SetupConnection(IPAddress ipAddress)
        {
            //IPAddress.Parse(TcpConnectionBase.ServerIp)
            IPEndPoint endPoint = new IPEndPoint(ipAddress, GameConstants.Port);
            Socket client = null;
            try
            {
                int i = 0;
                // retry login in case SocketError.ConnectionRefused / WSAECONNREFUSED / 10061
                for (i = 0; i < GameConstants.ConnectTryCount; i++)
                {
                    // Create a TCP/IP socket.
                    client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                    // Connect to the remote endpoint. 
                    var acyncResult = client.BeginConnect(endPoint, ConnectCallback, client);

                    // Wait
                    bool success =
                        acyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(GameConstants.ClientTimeToWait));
                    

                    if (!success)
                    {
                        ConnectionState = TcpConnectionState.Failed;
                        TcpHelper.LogWaitClientError();
                    }
                    else
                    {
                        success = _acceptEventHandler.WaitOne(TimeSpan.FromMilliseconds(GameConstants.ClientTimeToWait));
                        if (!success && ConnectionState!=TcpConnectionState.NeedRetry)
                        {
                            ConnectionState = TcpConnectionState.Failed;
                            TcpHelper.LogWaitClientError();
                        }
                    }
                    if (ConnectionState == TcpConnectionState.NeedRetry)
                    {
                        _acceptEventHandler.Reset();
                        Thread.Sleep(TimeSpan.FromMilliseconds(GameConstants.ClientTimeToWait));
                        ConnectionState = TcpConnectionState.NotInitialized;
                        client.Close();
                    }
                    else
                    {
                        // Successfully initialized
                        _acceptEventHandler.Close();
                        break;
                    }
                }
                if (i == GameConstants.ConnectTryCount)
                {
                    ConnectionState = TcpConnectionState.Failed;
                    TcpHelper.LogWaitClientError();
                }
            }
            catch (Exception e)
            {
                ConnectionState = TcpConnectionState.Failed;
                TcpHelper.LogSocketException(e);
                // Close Socket
                if (client != null)
                    client.Close();
                _acceptEventHandler.Close();
            }
        }

        private void ConnectCallback(IAsyncResult acyncResult)
        {
            // return if listener Closed
            if (ConnectionState == TcpConnectionState.Failed)
                return;
            // Retrieve the socket from the state object.  
            Socket client = (Socket)acyncResult.AsyncState;
            try
            {
                // Complete the connection.
                try
                {
                    client.EndConnect(acyncResult);
                }
                // In case SocketError.ConnectionRefused / WSAECONNREFUSED / 10061
                catch (SocketException e)
                {
                    ConnectionState = TcpConnectionState.NeedRetry;
                    TcpHelper.LogSocketException(e);
                }
                if (ConnectionState != TcpConnectionState.NeedRetry)
                {
                    ConnnectedSocket = client;
                    ConnectionState = TcpConnectionState.Connected;
                }
                _acceptEventHandler.Set();
            }
            catch (Exception e)
            {
                ConnectionState = TcpConnectionState.Failed;
                TcpHelper.LogSocketException(e);
                // Close Socket
                client.Close();
            }
        }
    }
}
