﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TcpTestProject
{
    class TcpHelper
    {
        public static void LogSocketException(Exception exception, bool discarded = false)
        {
            if (discarded)
            {
                Console.WriteLine("Socket discarded: you can ignore this exception");
            }
            if (exception is SocketException)
            {
                var socketException = exception as SocketException;
                Console.WriteLine($"{socketException.SocketErrorCode}: {exception}");
            }
            else Console.WriteLine(exception.ToString());
        }

        public static void LogException(Exception exception)
        {
            Console.WriteLine(exception.ToString());
        }

        public static void LogWaitServerError()
        {
            Console.WriteLine("Server waited too long to connect with client");
        }

        public static void LogWaitClientError()
        {
            Console.WriteLine("Client waited too long to connect to server");
        }

        public static void ConnectionIsNotInitialozed()
        {
            Console.WriteLine("Fail to make connection");
        }

        public static void Reconnecting()
        {
            Console.WriteLine("Reconnecting");
        }
    }
}
