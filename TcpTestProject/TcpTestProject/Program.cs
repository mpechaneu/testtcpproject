﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpTestProject
{
    // You should initialize GameConstants.ServerIp
    class Program
    {
        private static NetworkTcpService _clientConnector;
        private static NetworkTcpService _serverConnector;

        static void Main(string[] args)
        {
            Action callerClient = new Action(StartClient);
            var asClient = callerClient.BeginInvoke(null, null);
            Action callerServer = new Action(StartServer);
            var asServer = callerServer.BeginInvoke(null, null);

            asServer.AsyncWaitHandle.WaitOne();
            asClient.AsyncWaitHandle.WaitOne();

            if (_serverConnector.IsGameTerminated() || _clientConnector.IsGameTerminated())
            {
                Console.ReadLine();
                return;
            }

            Console.WriteLine("Ready to work");

            int i=0;
            for (i=0; i<5; i++)
            {
                _serverConnector.SendMessage(new TestMessage() {Name = ("Message1_"+i), Doubles = new List<double>() { 87, 56 } });
                Thread.Sleep(1000);
            }
            for (i = 0; i < 5; i++)
            {
                _clientConnector.SendMessage(new TestMessage() { Name = ("Message2_" + i.ToString()) });
                Thread.Sleep(1000);
            }

            Console.WriteLine("Finished");

            Console.ReadLine();
        }

        static void StartClient()
        {
            _clientConnector = new NetworkTcpService(false, IPAddress.Parse(GameConstants.ServerIp), new MessageQueue());
            _clientConnector.Initialize();
        }

        static void StartServer()
        {
            _serverConnector = new NetworkTcpService(true, IPAddress.Parse(GameConstants.ServerIp),new MessageQueue());
            _serverConnector.Initialize();
        }
    }
}
