﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TcpTestProject
{
    class TcpMessageService
    {
        private Timer _sendTimer = null;
        private Timer _timoutTimer = null;

        private TcpCommunicationService _tcpCommunicationService;
        private TcpConnectionBase _connectionBase;
        private readonly bool _server;
        private readonly IPAddress _ipAddress;
        private bool _suppressReconnect;
        private bool _reconnecting;

        public event Action StartRecconect;
        public event Action TerminateGameSession;
        public event Action SendFullGameState; //only for server!
        public event Action<byte[]> MessageArrived;

        public TcpMessageService(bool server, IPAddress ipAddress)
        {
            _server = server;
            _ipAddress = ipAddress;
            _suppressReconnect = false;
            _reconnecting = false;
        }

        public void SetupConnection()
        {
            var success = InitizeSocketsWithTimer();
            if (success)
            {
                InitializeTcpCommunicationService();
                if (GameConstants.LifetimeTurnedOn)
                    InitializeTimers();
            }
            else
            {
                OnTerminateGameSession();
            }
        }

        public void SendMessage(byte[] message)
        {
            if (_reconnecting)
                return;
            _tcpCommunicationService.SendMessage(message);
        }

        public void SuppressRecconect()
        {
            _suppressReconnect = true;
        }

        public void Close()
        {
            if (GameConstants.LifetimeTurnedOn)
                UninitializeTimers();
        }

        private void OnTerminateGameSession()
        {
            // Raise the event
            if (TerminateGameSession != null)
                TerminateGameSession();
        }

        private void OnSendFullGameState()
        {
            // Raise the event
            if (_server && SendFullGameState != null)
                SendFullGameState();
        }

        private void OnStartRecconect()
        {
            if (StartRecconect != null)
                StartRecconect();
        }

        private void InitializeTimers()
        {
            UninitializeTimers();
            _sendTimer = new Timer();
            _timoutTimer = new Timer();
            _sendTimer.Elapsed += OnSendTimedEvent;
            _timoutTimer.Elapsed += OnTimeOutEvent;
            _sendTimer.Interval = GameConstants.SendLifetimeInterval;
            _timoutTimer.Interval = GameConstants.LifetimeIntervalTimout;
            _sendTimer.Start();
            _timoutTimer.Start();
        }

        private void UninitializeTimers()
        {
            if (_sendTimer != null)
            {
                _sendTimer.Elapsed -= OnSendTimedEvent;
                _timoutTimer.Elapsed -= OnTimeOutEvent;
                _sendTimer.Close();
                _timoutTimer.Close();
                _sendTimer = null;
                _timoutTimer = null;
            }
        }
    
        private void OnSendTimedEvent(object source, ElapsedEventArgs e)
        {
            if (_reconnecting)
                return;
            _tcpCommunicationService.SendKeepAliveMessage();
        }

        private void OnTimeOutEvent(object source, ElapsedEventArgs e)
        {
            if (_reconnecting)
                return;
            _timoutTimer.Stop();
            OnNetworkError();
        }

        private void OnMessageArrived(byte[] message)
        {
            if (_reconnecting)
                return;
            if (message.Length == 0) // Is lifetime message
            {
                _timoutTimer.Stop();
                _timoutTimer.Start();
            }
            else
            {
                if (MessageArrived != null)
                    MessageArrived(message);
            }
        }

        private void OnNetworkError()
        {
            TcpHelper.Reconnecting();
            _reconnecting = true;
            _suppressReconnect = false;
            if (GameConstants.LifetimeTurnedOn)
                UninitializeTimers();
            _tcpCommunicationService.MessageArrived -= OnMessageArrived;
            _tcpCommunicationService.OnNetworkError -= OnNetworkError;
            _tcpCommunicationService.Discarded = true;
            _connectionBase = null;
            _tcpCommunicationService = null;
            OnStartRecconect();
            TryToReconnect();
            if (_suppressReconnect)
            {
                OnTerminateGameSession();
            }
            else
            {
                // reconnected
                InitializeTcpCommunicationService();
                if (GameConstants.LifetimeTurnedOn)
                    InitializeTimers();
                _reconnecting = false;
                _suppressReconnect = false;
                OnSendFullGameState();
            }
        }

        private bool InitizeSocketsWithTimer()
        {
            int i;
            for (i = 0; i < GameConstants.InitializeTryCount; i++)
            {
                InitializeConnectionBase();
                if (_connectionBase.ConnectionState == TcpConnectionState.Connected)
                    break;
            }
            if (i == GameConstants.InitializeTryCount)
                return false;
            return true;
        }

        private void TryToReconnect()
        {
            while (!_suppressReconnect)
            {
                InitializeConnectionBase();
                if (_connectionBase.ConnectionState == TcpConnectionState.Connected)
                    break;
            }
        }

        private void InitializeConnectionBase()
        {
            if (_server)
                _connectionBase = new TcpServerConnection();
            else _connectionBase = new TcpClientConnection();
            _connectionBase.SetupConnection(_ipAddress);
        }

        private void InitializeTcpCommunicationService()
        {
            _tcpCommunicationService = new TcpCommunicationService(_connectionBase.ConnnectedSocket);
            _tcpCommunicationService.MessageArrived += OnMessageArrived;
            _tcpCommunicationService.OnNetworkError += OnNetworkError;
            _tcpCommunicationService.StartReceive();
        }
    }
}
