﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TcpTestProject
{
    class TcpCommunicationService
    {
        private object _lockObj = new object();

        private readonly Socket _socket;
        private readonly PacketProtocol _packetProtocol;
        private readonly byte[] _buffer;
        private readonly Queue<byte[]> _messagesToSend;
        private bool _sending;
        private string socketHandle;

        // On error
        public event Action OnNetworkError;
        public event Action<byte[]> MessageArrived;
        public TcpConnectionState ConnectionState { get; private set; }
        public bool Discarded;

        public TcpCommunicationService(Socket socket)
        {
            _socket = socket;
            socketHandle = _socket.Handle.ToString();
            _packetProtocol = new PacketProtocol(0);
            _packetProtocol.MessageArrived += MessageArrivedInternal;
            _buffer = new byte[GameConstants.BufferSize];
            _messagesToSend = new Queue<byte[]>();
            ConnectionState = TcpConnectionState.Connected;
            _sending = false;
            Discarded = false;
        }

        public void SendMessage(byte[] message)
        {
            var wrappedMessage = PacketProtocol.WrapMessage(message);
            Send(wrappedMessage);
        }

        public void SendKeepAliveMessage()
        {
            var wrappedMessage = PacketProtocol.WrapKeepaliveMessage();
            Send(wrappedMessage);
        }

        public void StartReceive()
        {
            try
            {
                _socket.BeginReceive(_buffer, 0, _buffer.Length, 0,
                    ReceiveCallback, null);
            }
            catch (Exception exception)
            {
                TcpHelper.LogSocketException(exception, Discarded);
                if (!Discarded)
                    OnSocketCorrupted();
            }
        }


        private void ReceiveCallback(IAsyncResult ar)
        {
            if (ConnectionState == TcpConnectionState.Failed || ConnectionState == TcpConnectionState.Disconnected)
                return;
            try
            {
                int bytesCount = _socket.EndReceive(ar);
                if (bytesCount == 0 || Discarded)
                {
                    //OnDisconnect();
                    return;
                }
                byte[] destinationArray = new byte[bytesCount];
                Array.Copy(_buffer, destinationArray, bytesCount);
                _packetProtocol.DataReceived(destinationArray);
                _socket.BeginReceive(_buffer, 0, _buffer.Length, 0,
                    ReceiveCallback, null);
            }
            catch (Exception exception)
            {
                TcpHelper.LogSocketException(exception, Discarded);
                if (!Discarded)
                    OnSocketCorrupted();
            }
        }


        private void Send(byte[] message)
        {
            if (ConnectionState == TcpConnectionState.Failed || ConnectionState == TcpConnectionState.Disconnected)
                return;

            lock (_lockObj)
            {
                if (_sending)
                {
                    _messagesToSend.Enqueue(message);
                    return;
                }
                _sending = true;
            }

            try
            {
                _socket.BeginSend(message, 0, message.Length, 0,
                    SendCallback, null);
            }
            catch (Exception exception)
            {
                TcpHelper.LogSocketException(exception, Discarded);
                if (!Discarded)
                    OnSocketCorrupted();
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            if (ConnectionState == TcpConnectionState.Failed || ConnectionState == TcpConnectionState.Disconnected)
                return;
            try
            {
                // Complete sending the data to the remote device.  
                _socket.EndSend(ar);
                byte[] message;
                lock (_lockObj)
                {
                    if (_messagesToSend.Count == 0)
                    {
                        _sending = false;
                        return;
                    }
                    message = _messagesToSend.Dequeue();
                }
                _socket.BeginSend(message, 0, message.Length, 0,
                    SendCallback, null);
            }
            catch (Exception exception)
            {
                TcpHelper.LogSocketException(exception, Discarded);
                if (!Discarded)
                    OnSocketCorrupted();
            }
        }

        public void Disconnect()
        {
            if (ConnectionState == TcpConnectionState.Failed || ConnectionState == TcpConnectionState.Disconnected)
                return;
            try
            {
                _socket.BeginDisconnect(false, DisconnectCallback, null);
            }
            catch (Exception exception)
            {
                TcpHelper.LogSocketException(exception);
                ConnectionState = TcpConnectionState.Disconnected;
                _socket.Close();
            }
        }



        private void DisconnectCallback(IAsyncResult ar)
        {
            if (ConnectionState == TcpConnectionState.Failed || ConnectionState == TcpConnectionState.Disconnected)
                return;
            try
            {
                _socket.EndDisconnect(ar);
                OnDisconnect();
            }
            catch (Exception exception)
            {
                TcpHelper.LogSocketException(exception);
                OnDisconnect();
            }
        }

        private void OnSocketCorrupted()
        {
            ConnectionState = TcpConnectionState.Failed;
            // Raise the event
            if (OnNetworkError != null)
                OnNetworkError();
        }

        private void OnDisconnect()
        {
            ConnectionState = TcpConnectionState.Disconnected;
            _socket.Close();
        }

        private void MessageArrivedInternal(byte[] dataBytes)
        {
            // Zero-length packets are allowed as keepalives
            if (MessageArrived != null)
                MessageArrived(dataBytes);
        }
    }
}
