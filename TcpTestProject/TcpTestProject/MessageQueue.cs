﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpTestProject
{
    class MessageQueue
    {
        private readonly ConcurrentQueue<MessageBase> _queue;  

        public MessageQueue()
        {
            _queue = new ConcurrentQueue<MessageBase>();
        }

        public void AddMessage(MessageBase message)
        {
            _queue.Enqueue(message);
        }

        public MessageBase TryGetMessage()
        {
            MessageBase message = null;
            _queue.TryDequeue(out message);
            return message;
        }
    }
}
